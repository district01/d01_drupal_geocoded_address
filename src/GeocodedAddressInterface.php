<?php

namespace Drupal\d01_drupal_geocoded_address;

use Geocoder\Model\Address;

/**
 * Interface GeocodedAddressInterface.
 *
 * @package Drupal\d01_drupal_geocoded_address
 */
interface GeocodedAddressInterface {

  /**
   * Constructor.
   *
   * @param \Geocoder\Model\Address $address
   *   A geocoded address model.
   * @param string|null $location_name
   *   The location name.
   */
  public function __construct(Address $address, string $location_name);

  /**
   * Get the geocoded address object.
   *
   * @return \Geocoder\Model\Address
   *   A geocoded address model.
   */
  public function getAddress();

  /**
   * Get the location name.
   *
   * @return string
   *   A location name.
   */
  public function getLocationName();

  /**
   * Get the address latitude.
   *
   * @return float
   *   A latitude point.
   */
  public function getLatitude();

  /**
   * Get the address longitude.
   *
   * @return float
   *   A longitude point.
   */
  public function getLongitude();

  /**
   * Get the spatial coordinates.
   *
   * @return string|bool
   *   The coordinates as string separated by a comma.
   */
  public function getSpatial();

  /**
   * Format the address.
   *
   * Mappings :
   * https://github.com/geocoder-php/Geocoder.
   *
   * Location Name: %N
   * Street Number: %n
   * Street Name: %S
   * City (Locality): %L
   * City District (Sub-Locality): %D
   * Zipcode (Postal Code): %z
   * Country: %C
   * Country Code: %c
   * Timezone: %T
   *
   * @return string
   *   A formatted address string.
   */
  public function format();

}
