<?php

namespace Drupal\d01_drupal_geocoded_address;

use Drupal\geocoder\ProviderPluginManager;
use Drupal\geocoder\Geocoder;

/**
 * Class GeocodeService.
 *
 * @package Drupal\d01_drupal_geocoded_address
 */
class GeocodeService implements GeocodeServiceInterface {

  // For now we limit the providers, later on this will come from config.
  private static $usedProviders = [
    'googlemaps',
    'bingmaps',
    'mapquest',
    'openstreetmap',
    'tomtom',
    'yandex',
  ];

  /**
   * The geocoder provider plugin manager service.
   *
   * @var \Drupal\geocoder\ProviderPluginManager
   */
  protected $providerPluginManager;

  /**
   * The geocoder service.
   *
   * @var \Drupal\geocoder\Geocoder
   */
  protected $geocoder;

  /**
   * {@inheritdoc}
   */
  public function __construct(ProviderPluginManager $provider_plugin_manager, Geocoder $geocoder) {
    $this->providerPluginManager = $provider_plugin_manager;
    $this->geocoder = $geocoder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @see \Drupal\Core\Field\WidgetPluginManager::createInstance().
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('plugin.manager.geocoder.provider'),
      $container->get('current_route_match'),
      $container->get('geocoder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function toGeocodableString($street_name = NULL, $street_number = NULL, $postal_code = NULL, $locality = NULL, $country_code = NULL) {
    $address_parts_1 = [];
    $address_parts_2 = [];

    if (!$street_name || !$postal_code || !$locality || !$country_code) {
      return NULL;
    }

    // Combine street and number.
    $address_parts_1[] = $street_name;
    $address_parts_1[] = $street_number;

    // Combine postal code, locality and country code.
    $address_parts_2[] = $postal_code;
    $address_parts_2[] = $locality;
    $address_parts_2[] = $country_code;

    $address_line = implode(' ', $address_parts_1);
    $address_line .= ', ';
    $address_line .= implode(' ', $address_parts_2);

    return $address_line;
  }

  /**
   * {@inheritdoc}
   */
  public function geocodeAddress($address_line) {
    if (!$address_line) {
      return NULL;
    }

    $plugin_list = [];
    $options = [];

    $plugins = $this->providerPluginManager->getPlugins();
    foreach ($plugins as $key => $plugin) {
      if (!in_array($plugin['id'], self::$usedProviders)) {
        continue;
      }

      array_push($plugin_list, $plugin['id']);
      $options[$plugin['id']] = $plugin['arguments'];
    }

    try {
      $collection = $this->geocoder->geocode($address_line, $plugin_list, $options);
      $address = $collection ? $collection->first() : NULL;
      return $address;
    }
    catch (\Exception $e) {
      return NULL;
    }
  }

}
