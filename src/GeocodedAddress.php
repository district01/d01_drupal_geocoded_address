<?php

namespace Drupal\d01_drupal_geocoded_address;

use Geocoder\Model\Address;
use Geocoder\Formatter\StringFormatter;

/**
 * GeocodedAddress Class.
 *
 * Builds an address object with geocoding information
 * It will use the Geocoder\Model\Address and all unknown methods
 * are passed to this object.
 */
class GeocodedAddress {

  /**
   * The address.
   *
   * @var \Geocoder\Model\Address
   */
  protected $address;

  /**
   * The location name.
   *
   * @var string|null
   */
  protected $locationName;

  /**
   * {@inheritdoc}
   */
  public function __construct(Address $address, string $location_name = NULL) {
    $this->address = $address;
    $this->locationName = $location_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getAddress() {
    return $this->address;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocationName() {
    return $this->locationName;
  }

  /**
   * {@inheritdoc}
   */
  public function getLatitude() {
    if (!is_null($this->address->getCoordinates())) {
      return $this->address->getCoordinates()->getLatitude();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLongitude() {
    if (!is_null($this->address->getCoordinates())) {
      return $this->address->getCoordinates()->getLongitude();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getSpatial() {
    if ($this->getLongitude() && $this->getLatitude()) {
      return $this->getLatitude() . ',' . $this->getLongitude();
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function format($format) {

    if (!$this->getAddress()) {
      return FALSE;
    }

    // Get country (if available).
    if (!is_null($country = $this->address->getCountry())) {
      $country = $this->address->getCountry()->getName();

      // When formatting contains country replace it.
      // This way the country will be translatable.
      $format = strtr($format, ['%C' => $country]);
    }

    // Since we extended the address with a location name
    // we also have to support an extra formatter parameter.
    $format = strtr($format, ['%N' => $this->getLocationName()]);

    // Initialize new formatter.
    $formatter = new StringFormatter();
    return $formatter->format($this->getAddress(), $format);
  }

}
