<?php

namespace Drupal\d01_drupal_geocoded_address;

use Drupal\geocoder\ProviderPluginManager;
use Drupal\geocoder\Geocoder;

/**
 * Interface GeocodeServiceInterface.
 *
 * @package Drupal\d01_drupal_geocoded_address
 */
interface GeocodeServiceInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\geocoder\ProviderPluginManager $provider_plugin_manager
   *   The geocoder provider plugin manager service.
   * @param \Drupal\geocoder\Geocoder $geocoder
   *   The geocoder service.
   */
  public function __construct(ProviderPluginManager $provider_plugin_manager, Geocoder $geocoder);

  /**
   * Format to string for geocoding.
   *
   * @param string $street_name
   *   The street name.
   * @param string $street_number
   *   The street number.
   * @param string $postal_code
   *   The postal code.
   * @param string $locality
   *   The locality.
   * @param string $country_code
   *   The country code.
   */
  public function toGeocodableString($street_name = NULL, $street_number = NULL, $postal_code = NULL, $locality = NULL, $country_code = NULL);

  /**
   * Geocode an address.
   *
   * @param string $formatted_address
   *   A formatted address string.
   *
   * @return null|\Geocoder\Model\Address
   *   An address model.
   */
  public function geocodeAddress($formatted_address);

}
