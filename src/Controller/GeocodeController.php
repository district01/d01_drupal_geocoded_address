<?php

namespace Drupal\d01_drupal_geocoded_address\Controller;

use Drupal\Core\Controller\ControllerBase;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\d01_drupal_geocoded_address\GeocodeService;
use Drupal\jsonapi\Resource\JsonApiDocumentTopLevel;
use Drupal\jsonapi\ResourceResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Serialization\Json;

/**
 * Class GeocodeController.
 *
 * @package Drupal\d01_drupal_geocoded_address\Controller
 */
class GeocodeController extends ControllerBase {

  /**
   * @var \Drupal\d01_drupal_geocoded_address\GeocodeService
   */
  protected $geocoder;

  /**
   * Constructor.
   *
   * @param \Drupal\d01_drupal_geocoded_address\GeocodeService $geocoder
   *   The geocoder service.
   */
  public function __construct(GeocodeService $geocoder) {
    $this->geocoder = $geocoder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('d01_drupal_geocoded_address.geocode')
    );
  }

  /**
   * Processes POST requests to /d01-drupal-geocoded-address/geocode.
   *
   * Will support request of an address as string and an address as object.
   * When a request contains both an address as string and an address as object
   * the string will be used for geocoding.
   *
   * ex :
   *  {
   *    "address" : "Street 1, 2000 City"
   *  }
   *
   * {
   *    "street_name" : "Street",
   *    "street_number" : "1",
   *    "postal_code" : "2000",
   *    "locality" : "City",
   *    "country_code" : "BE",
   * }
   */
  public function geocode(ServerRequestInterface $request, $response_code = 200) {

    // Support raw posts.
    $parsed_response = Json::decode($request->getBody()->getContents());

    // Support form-data & x-www-form-urlencoded posts.
    if (empty($parsed_response)) {
      $parsed_response = $request->getParsedBody();
    }

    // Check if we need to build the address ourselfs or a
    // formatted string is passed.
    $is_split = isset($parsed_response['address']) && !empty($parsed_response['address']) ? FALSE : TRUE;
    if ($is_split) {
      $street_name = isset($parsed_response['street_name']) ? $parsed_response['street_name'] : FALSE;
      $street_nr = isset($parsed_response['street_number']) ? $parsed_response['street_number'] : FALSE;
      $postal_code = isset($parsed_response['postal_code']) ? $parsed_response['postal_code'] : FALSE;
      $locality = isset($parsed_response['locality']) ? $parsed_response['locality'] : FALSE;
      $country_code = isset($parsed_response['country_code']) ? $parsed_response['country_code'] : FALSE;
      $address_to_geocode = $this->geocoder->toGeocodableString($street_name, $street_nr, $postal_code, $locality, $country_code);
    }
    else {
      $address_to_geocode = isset($parsed_response['address']) ? $parsed_response['address'] : FALSE;
    }

    // Call geocoder service.
    $geocoded_address = $this->geocoder->geocodeAddress($address_to_geocode);

    // Build a response.
    $response = [];
    if ($geocoded_address) {

      // We can't use the toArray() method since we want our keys to
      // be consistent with the table names in our field.
      $response = [
        'bound_east' => $geocoded_address->getBounds()->getEast(),
        'bound_north' => $geocoded_address->getBounds()->getNorth(),
        'bound_south' => $geocoded_address->getBounds()->getSouth(),
        'bound_west' => $geocoded_address->getBounds()->getWest(),
        'country_code' => $geocoded_address->getCountryCode(),
        'country' => $geocoded_address->getCountry()->getName(),
        'latitude' => $geocoded_address->getLatitude(),
        'longitude' => $geocoded_address->getLongitude(),
        'postal_code' => $geocoded_address->getPostalCode(),
        'street_name' => $geocoded_address->getStreetName(),
        'street_number' => $geocoded_address->getStreetNumber(),
        'sub_locality' => $geocoded_address->getSubLocality(),
        'timezone' => $geocoded_address->getTimezone(),
      ];
    }

    return $this->buildWrappedResponse($response, $response_code);
  }

  /**
   * Builds a response with the appropriate wrapped document.
   *
   * @param mixed $data
   *   The data to wrap.
   * @param int $response_code
   *   The response code.
   * @param array $headers
   *   An array of response headers.
   *
   * @return \Drupal\jsonapi\ResourceResponse
   *   The response.
   */
  protected function buildWrappedResponse($data, $response_code = 200, array $headers = []) {
    return new ResourceResponse($data, $response_code, $headers);
  }

}
