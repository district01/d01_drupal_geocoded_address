<?php

namespace Drupal\d01_drupal_geocoded_address;

use Drupal\Core\TypedData\TypedData;

/**
 * A computed property for spatial.
 */
class SpatialComputed extends TypedData {

  /**
   * Cached spatial string.
   *
   * @var string|null
   */
  protected $spatial = NULL;

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if ($this->spatial !== NULL) {
      return $this->spatial;
    }

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    $item = $this->getParent();
    $geocoded_address = $item->toGeocodedAddress();

    $this->spatial = $geocoded_address->getSpatial();
    return $this->spatial;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    $this->spatial = $value;
    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->spatial);
    }
  }

}
