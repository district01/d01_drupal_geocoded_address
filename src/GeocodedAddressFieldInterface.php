<?php

namespace Drupal\d01_drupal_geocoded_address;

use Drupal\Core\Field\FieldItemInterface;

/**
 * Interface GeocodedAddressFieldInterface.
 *
 * @package Drupal\d01_drupal_geocoded_address
 */
interface GeocodedAddressFieldInterface extends FieldItemInterface {

  /**
   * Gets the GeocodedAddress object for the current field item.
   *
   * @return \Drupal\d01_drupal_geocoded_address\GeocodedAddressInterface
   *   A geocoded address object.
   */
  public function toGeocodedAddress();

}

