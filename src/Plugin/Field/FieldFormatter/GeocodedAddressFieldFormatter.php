<?php

namespace Drupal\d01_drupal_geocoded_address\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Formatter for "Geocoded Address" field.
 *
 * @FieldFormatter(
 *   id = "d01_drupal_geocoded_address_formatter",
 *   label = @Translation("Geocoded Address Formatter"),
 *   field_types = {
 *     "d01_drupal_geocoded_address"
 *   }
 * )
 */
class GeocodedAddressFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $address = $item->value;
      $lat = $address->getLatitude();
      $lng = $address->getLongitude();
      $coordinates = $this->t('No coordinates found');
      if ($lat && $lng) {
        $coordinates = $lat . ' , ' . $lng;
      }

      if ($address->getLocationName()) {
        $format = '%N, %S %n, %z %L %C';
      }
      else {
        $format = '%S %n, %z %L %C';
      }

      $element[$delta] = [
        '#markup' => $address->format($format) . ' ( ' . $coordinates . ' )',
      ];
    }

    return $element;
  }

}
