<?php

namespace Drupal\d01_drupal_geocoded_address\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Locale\CountryManager;
use Drupal\d01_drupal_geocoded_address\GeocodedAddressFieldInterface;
use Geocoder\Model\Address;
use Geocoder\Model\Coordinates;
use Geocoder\Model\Country;
use Geocoder\Model\Bounds;
use Drupal\d01_drupal_geocoded_address\GeocodedAddress;
use Geocoder\Model\AdminLevelCollection;

/**
 * Plugin implementation of the 'address' field type.
 *
 * @FieldType(
 *   id = "d01_drupal_geocoded_address",
 *   label = @Translation("Geocoded Address"),
 *   category = @Translation("Geocoded Address"),
 *   default_widget = "d01_drupal_geocoded_address_widget",
 *   default_formatter = "d01_drupal_geocoded_address_formatter"
 * )
 */
class GeocodedAddressField extends FieldItemBase implements GeocodedAddressFieldInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'countries' => [],
        'optional_fields' => [],
        'geocode_feedback' => 'basic_warning',
      ] + parent::defaultFieldSettings();
  }

  /**
   * Get all the countries.
   *
   * @return array
   *   An option list of countries.
   */
  public function getCountries() {
    $countries = [];
    foreach (CountryManager::getStandardList() as $key => $value) {
      $countries[$key] = $value->__toString();
    }
    return $countries;
  }

  /**
   * Get all the optional fields.
   *
   * @return array
   *   An option list of fields.
   */
  public function getOptionalField() {
    return [
      'location_name' => $this->t('Location name'),
      'sub_locality' => $this->t('District'),
    ];
  }

  /**
   * Get all types of feedback.
   *
   * @return array
   *   An option list of feedback.
   */
  public function getFeedbackMessage() {
    return [
      'none' => $this->t('None'),
      'basic_warning' => $this->t('Basic warning'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);

    $defaults = $this->defaultFieldSettings();

    $enabled_countries = $this->getSetting('countries') ? array_filter($this->getSetting('countries')) : $defaults['countries'];
    $form['countries'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Countries'),
      '#description' => $this->t('Check all countries you want to enable, when no countries are selected all countries will be enabled.'),
      '#options' => $this->getCountries(),
      '#default_value' => $enabled_countries,
    ];

    $optional_fields = $this->getSetting('optional_fields') ? array_filter($this->getSetting('optional_fields')) : $defaults['optional_fields'];
    $form['optional_fields'] = [
      '#title' => $this->t('Optional fields'),
      '#description' => $this->t('These fields are not used for geocoding the address, but can be useful to collect. Check the fields you want to collect.'),
      '#type' => 'checkboxes',
      '#options' => $this->getOptionalField(),
      '#default_value' => $optional_fields,
    ];

    $geocode_feedback = $this->getSettings('geocode_feedback') ? $this->getSettings('geocode_feedback') : $defaults['geocode_feedback'];
    $form['geocode_feedback'] = [
      '#title' => $this->t('Geocode failure feedback'),
      '#description' => $this->t('Type of feedback to show the user when geocoding fails.'),
      '#type' => 'select',
      '#options' => $this->getFeedbackMessage(),
      '#default_value' => $geocode_feedback,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'latitude' => [
          'description' => 'Stores the latitude value from geocoder service',
          'type' => 'float',
          'size' => 'big',
          'not null' => FALSE,
        ],
        'longitude' => [
          'description' => 'Stores the longitude value from geocoder service',
          'type' => 'float',
          'size' => 'big',
          'not null' => FALSE,
        ],
        'locked' => [
          'description' => 'Boolean to indicate whether the coordinates are locked or should be re-geocoded on save',
          'type' => 'int',
          'size' => 'tiny',
          'default' => 0,
        ],
        'bound_south' => [
          'description' => 'Stores the south bound from geocoder service',
          'type' => 'float',
          'size' => 'big',
          'not null' => FALSE,
        ],
        'bound_west' => [
          'description' => 'Stores the west bound from geocoder service',
          'type' => 'float',
          'size' => 'big',
          'not null' => FALSE,
        ],
        'bound_north' => [
          'description' => 'Stores the north bound from geocoder service',
          'type' => 'float',
          'size' => 'big',
          'not null' => FALSE,
        ],
        'bound_east' => [
          'description' => 'Stores the east bound from geocoder service',
          'type' => 'float',
          'size' => 'big',
          'not null' => FALSE,
        ],
        'location_name' => [
          'description' => 'Stores the name of the location from user input',
          'type' => 'varchar',
          'length' => 255,
        ],
        'street_number' => [
          'description' => 'Stores the street number from user input',
          'type' => 'varchar',
          'length' => 255,
        ],
        'street_name' => [
          'description' => 'Stores the street name from user input',
          'type' => 'varchar',
          'length' => 255,
        ],
        'sub_locality' => [
          'description' => 'Stores the sub locality from user input',
          'type' => 'varchar',
          'length' => 255,
        ],
        'locality' => [
          'description' => 'Stores the locality from user input',
          'type' => 'varchar',
          'length' => 255,
        ],
        'postal_code' => [
          'description' => 'Stores the postal code from user input',
          'type' => 'varchar',
          'length' => 255,
        ],
        'country_code' => [
          'description' => 'Stores the country code from geocoder service',
          'type' => 'varchar',
          'length' => 2,
        ],
        'timezone' => [
          'description' => 'Stores the timezone from geocoder service',
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'latitude' => ['latitude'],
        'longitude' => ['longitude'],
        'coordinates' => [
          'latitude',
          'longitude',
        ],
        'bounds' => [
          'bound_south',
          'bound_west',
          'bound_north',
          'bound_east',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['value'] = DataDefinition::create('any')
      ->setLabel(t('Computed Geocoded Address'))
      ->setDescription(t('The computed Geocoded Address object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\d01_drupal_geocoded_address\GeocodedAddressComputed');

    $properties['latitude'] = DataDefinition::create('float')
      ->setLabel(t('The latitude.'));

    $properties['longitude'] = DataDefinition::create('float')
      ->setLabel(t('The longitude.'));

    $properties['locked'] = DataDefinition::create('boolean')
      ->setLabel(t('Boolean indicating if coordinates are locked.'));

    $properties['bound_south'] = DataDefinition::create('float')
      ->setLabel(t('The south bound.'));

    $properties['bound_west'] = DataDefinition::create('float')
      ->setLabel(t('The west bound.'));

    $properties['bound_north'] = DataDefinition::create('float')
      ->setLabel(t('The north bound.'));

    $properties['bound_east'] = DataDefinition::create('float')
      ->setLabel(t('The east bound.'));

    $properties['location_name'] = DataDefinition::create('string')
      ->setLabel(t('The name of the location.'));

    $properties['street_number'] = DataDefinition::create('string')
      ->setLabel(t('The street number.'));

    $properties['street_name'] = DataDefinition::create('string')
      ->setLabel(t('The street name.'));

    $properties['sub_locality'] = DataDefinition::create('string')
      ->setLabel(t('The sub locality.'));

    $properties['locality'] = DataDefinition::create('string')
      ->setLabel(t('The locality.'));

    $properties['postal_code'] = DataDefinition::create('string')
      ->setLabel(t('The postal code.'));

    $properties['country_code'] = DataDefinition::create('string')
      ->setLabel(t('The country code.'));

    $properties['timezone'] = DataDefinition::create('string')
      ->setLabel(t('The address timezone.'));

    $properties['spatial'] = DataDefinition::create('string')
      ->setLabel(t('Spatial'))
      ->setDescription(t('The combined coordinates for spatial searches.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\d01_drupal_geocoded_address\SpatialComputed');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if (!$this->getLocality() && !$this->getStreetName() && !$this->getPostalCode()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    // Enforce that the computed geocoded address is recalculated.
    if ($property_name == 'value') {
      $this->value = NULL;
      $this->spatial = NULL;
    }
    parent::onChange($property_name, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function toGeocodedAddress() {
    return $this->getGeocodedAddress();
  }

  /**
   * Get the latitude.
   *
   * @return double|null
   *   A latitude.
   */
  public function getLatitude() {
    return $this->latitude ? (double) $this->latitude : NULL;
  }

  /**
   * Get the longitude.
   *
   * @return double|null
   *   A longitude.
   */
  public function getLongitude() {
    return $this->longitude ? (double) $this->longitude : NULL;
  }

  /**
   * Get locked.
   *
   * @return bool
   *   An indication if coords are locked.
   */
  public function getLocked() {
    return $this->locked ? $this->locked : FALSE;
  }

  /**
   * Get the south bound.
   *
   * @return double|null
   *   The south bound.
   */
  public function getBoundSouth() {
    return $this->bound_south ? (double) $this->bound_south : NULL;
  }

  /**
   * Get the west bound.
   *
   * @return double|null
   *   The west bound.
   */
  public function getBoundWest() {
    return $this->bound_west ? (double) $this->bound_west : NULL;
  }

  /**
   * Get the north bound.
   *
   * @return double|null
   *   The north bound.
   */
  public function getBoundNorth() {
    return $this->bound_north ? (double) $this->bound_north : NULL;
  }

  /**
   * Get the east bound.
   *
   * @return double|null
   *   The east bound.
   */
  public function getBoundEast() {
    return $this->bound_east ? (double) $this->bound_east : NULL;
  }

  /**
   * The spatial.
   *
   * @return string|null
   *   The spatial.
   */
  public function getSpatial() {
    return $this->spatial ? (string) $this->spatial : NULL;
  }

  /**
   * The street number.
   *
   * @return string|null
   *   The street number.
   */
  public function getStreetNumber() {
    return $this->street_number ? (string) $this->street_number : NULL;
  }

  /**
   * The street name.
   *
   * @return string|null
   *   The street name.
   */
  public function getStreetName() {
    return $this->street_name ? (string) $this->street_name : NULL;
  }

  /**
   * The sub locality.
   *
   * @return string|null
   *   The sub locality.
   */
  public function getSubLocality() {
    return $this->sub_locality ? (string) $this->sub_locality : NULL;
  }

  /**
   * The locality.
   *
   * @return string|null
   *   The locality.
   */
  public function getLocality() {
    return $this->locality ? (string) $this->locality : NULL;
  }

  /**
   * The postal code.
   *
   * @return string|null
   *   The postal code.
   */
  public function getPostalCode() {
    return $this->postal_code ? (string) $this->postal_code : NULL;
  }

  /**
   * The timezone.
   *
   * @return string|null
   *   The timezone.
   */
  public function getTimezone() {
    return $this->timezone ? (string) $this->timezone : NULL;
  }

  /**
   * Get the country name.
   *
   * @return string|null
   *   The country name.
   */
  public function getCountryName() {
    $county_codes = CountryManager::getStandardList();
    return $this->getCountryCode() && isset($county_codes[$this->getCountryCode()]) ? $county_codes[$this->getCountryCode()] : NULL;
  }

  /**
   * The country code.
   *
   * @return string|null
   *   The country code.
   */
  public function getCountryCode() {
    return $this->country_code ? (string) $this->country_code : NULL;
  }

  /**
   * The location name.
   *
   * @return string|null
   *   The location name.
   */
  public function getLocationName() {
    return $this->location_name ? (string) $this->location_name : NULL;
  }

  /**
   * Get the geocoded address.
   *
   * @return \Drupal\d01_drupal_geocoded_address\GeocodedAddress|null
   *   The geocoded address object.
   */
  public function getGeocodedAddress() {
    try {
      return new GeocodedAddress(
        $this->getAddressModel(),
        $this->getLocationName()
      );
    }
    catch (\Exception $e) {
      return NULL;
    }
  }

  /**
   * Get the address model.
   *
   * @return \Geocoder\Model\Address|null
   *   The address object.
   */
  public function getAddressModel() {
    try {
      return new Address(
        'custom',
        new AdminLevelCollection(),
        $this->getCoordinatesModel(),
        $this->getBoundsModel(),
        $this->getStreetNumber(),
        $this->getStreetName(),
        $this->getPostalCode(),
        $this->getLocality(),
        $this->getSubLocality(),
        NULL,
        $this->getCountryModel(),
        $this->getTimezone()
      );
    }
    catch (\Exception $e) {
      return NULL;
    }
  }

  /**
   * Get the coordinates model.
   *
   * @return \Geocoder\Model\Coordinates|null
   *   A Coordinates object.
   */
  public function getCoordinatesModel() {
    try {
      return new Coordinates(
        $this->getLatitude(),
        $this->getLongitude()
      );
    }
    catch (\Exception $e) {
      return NULL;
    }
  }

  /**
   * Get the bounds model.
   *
   * @return \Geocoder\Model\Bounds|null
   *   The bounds object.
   */
  public function getBoundsModel() {
    try {
      return new Bounds(
        $this->getBoundSouth(),
        $this->getBoundWest(),
        $this->getBoundNorth(),
        $this->getBoundEast()
      );
    }
    catch (\Exception $e) {
      return NULL;
    }
  }

  /**
   * Get the country model.
   *
   * @return \Geocoder\Model\Country|null
   *   The country object.
   */
  public function getCountryModel() {
    try {
      return new Country(
        $this->getCountryName(),
        $this->getCountryCode()
      );
    }
    catch (\Exception $e) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    // For now DI is not supported in FieldTypes, so until
    // https://www.drupal.org/project/drupal/issues/2053415 is resolved
    // we use a \Drupal::service to get the geocoding service.
    $geocoder = \Drupal::service('d01_drupal_geocoded_address.geocode');;
    $address_to_geocode = $geocoder->toGeocodableString(
      $this->getStreetName(),
      $this->getStreetNumber(),
      $this->getPostalCode(),
      $this->getLocality(),
      $this->getCountryCode()
    );

    $geocoded_addresss = $geocoder->geocodeAddress($address_to_geocode);
    if ($geocoded_addresss) {

      // When latitude and longitude are locked we don't re-geocode
      // the provided address but we asume the passed coordinates are correct.
      if ($this->getLocked()) {
        $this->setLatitude($this->getLatitude());
        $this->setLongitude($this->getLongitude());
      }
      else {
        $this->setLatitude($geocoded_addresss->getCoordinates()->getLatitude());
        $this->setLongitude($geocoded_addresss->getCoordinates()->getLongitude());
      }

      // Set spatial.
      $spatial = $geocoded_addresss->getCoordinates()->getLatitude() . ',' .  $geocoded_addresss->getCoordinates()->getLongitude();
      $this->setSpatial($spatial);

      // Set the bounds.
      $bounds = $geocoded_addresss->getBounds();
      $this->setBoundSouth($bounds->getSouth());
      $this->setBoundEast($bounds->getEast());
      $this->setBoundNorth($bounds->getNorth());
      $this->setBoundWest($bounds->getWest());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    parent::postSave($update);
    $address = $this->value;
    $geocode_feedback = $this->getSettings('geocode_feedback');

    if ($address && (!$address->getLatitude() || !$address->getLongitude()) && $geocode_feedback === "basic_warning" ) {
      \Drupal::messenger()->addWarning($this->t('No coordinates were found for the specified address.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Allow callers to pass a GeocodedAddress value object
    // as the field item value.
    if ($values instanceof GeocodedAddress) {
      $address = $values;
      $values = [
        'latitude' => $address->getLatitude(),
        'longitude' => $address->getLongitude(),
        'bound_south' => $address->getAddress()->getBounds()->getSouth(),
        'bound_west' => $address->getAddress()->getBounds()->getWest(),
        'bound_north' => $address->getAddress()->getBounds()->getNorth(),
        'bound_east' => $address->getAddress()->getBounds()->getEast(),
        'location_name' => $address->getLocationName(),
        'street_number' => $address->getAddress()->getStreetNumber(),
        'street_name' => $address->getAddress()->getStreetName(),
        'sub_locality' => $address->getAddress()->getSubLocality(),
        'locality' => $address->getAddress()->getLocality(),
        'postal_code' => $address->getAddress()->getPostalCode(),
        'country_code' => $address->getAddress()->getCountryCode(),
        'timezone' => $address->getAddress()->getTimezone(),
      ];
    }

    parent::setValue($values, $notify);
  }

  /**
   * Set the latitude.
   *
   * @param double|null $latitude
   *   A latitude.
   */
  private function setLatitude($latitude) {
    $this->latitude = $latitude ? (double) $latitude : $latitude;
  }

  /**
   * Set the longitude.
   *
   * @param double|null $longitude
   *   A latitude.
   */
  private function setLongitude($longitude) {
    $this->longitude = $longitude ? (double) $longitude : $longitude;
  }

  /**
   * Set the bound south.
   *
   * @param double|null $bound_south
   *   A south bound.
   */
  private function setBoundSouth($bound_south) {
    $this->bound_south = $bound_south ? (double) $bound_south : $bound_south;
  }

  /**
   * Set the bound west.
   *
   * @param double|null $bound_west
   *   A west bound.
   */
  private function setBoundWest($bound_west) {
    $this->bound_west = $bound_west ? (double) $bound_west : $bound_west;
  }

  /**
   * Set the bound north.
   *
   * @param double|null $bound_north
   *   A north bound.
   */
  private function setBoundNorth($bound_north) {
    $this->bound_north = $bound_north ? (double) $bound_north : $bound_north;
  }

  /**
   * Set the bound east.
   *
   * @param double|null $bound_east
   *   A east bound.
   */
  private function setBoundEast($bound_east) {
    $this->bound_east = $bound_east ? (double) $bound_east : $bound_east;
  }

  /**
   * Set the spatial.
   *
   * @param string|null $spatial
   *   A spatial string.
   */
  private function setSpatial($spatial) {
    $this->spatial = $spatial ? (string) $spatial : $spatial;
  }

}
