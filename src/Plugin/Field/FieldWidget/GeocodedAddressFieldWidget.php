<?php

namespace Drupal\d01_drupal_geocoded_address\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Widget for "Geocoded Address" field.
 *
 * @FieldWidget(
 *   id = "d01_drupal_geocoded_address_widget",
 *   label = @Translation("Geocoded Address Widget"),
 *   field_types = {
 *     "d01_drupal_geocoded_address"
 *   }
 * )
 */
class GeocodedAddressFieldWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * Constructs an AddressDefaultWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   The route match service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ConfigFactoryInterface $config_factory, CurrentRouteMatch $route_match) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->configFactory = $config_factory;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @see \Drupal\Core\Field\WidgetPluginManager::createInstance().
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('current_route_match')
    );
  }

  /**
   * Get the countries.
   *
   * @param array $country_codes
   *   List of country codes.
   *
   * @return array
   *   Option list of countries.
   */
  public function getCountryOptions(array $country_codes = []) {
    $countries = [];
    foreach (CountryManager::getStandardList() as $key => $value) {
      if (in_array($key, $country_codes)) {
        $countries[$key] = $value->__toString();
      }
    }
    return $countries;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];

    // Defaults.
    $field_definitions = $item->getFieldDefinition();
    $settings = $field_definitions->getSettings();
    $default_literals = $field_definitions->getDefaultValueLiteral();
    $default_values = isset($default_literals[$delta]) ? $default_literals[$delta] : FALSE;

    $optional_fields = isset($settings['optional_fields']) ? array_filter($settings['optional_fields']) : [];
    $countries = isset($settings['countries']) ? array_filter($settings['countries']) : [];

    $element = [
      '#type' => 'fieldset',
    ] + $element;

    // Location.
    $location_name = NULL;
    if (isset($items[$delta]->location_name)) {
      $location_name = $items[$delta]->location_name;
    }
    elseif (isset($default_values['location_name'])) {
      $location_name = $default_values['location_name'];
    }

    if (in_array('location_name', $optional_fields)) {
      $element['location_name'] = [
        '#title' => t('Location Name'),
        '#type' => 'textfield',
        '#default_value' => $location_name,
      ];
    }
    else {
      $element['location_name'] = [
        '#type' => 'value',
        '#default_value' => FALSE,
      ];
    }

    // Country code.
    $country_code = NULL;
    if (isset($items[$delta]->country_code)) {
      $country_code = $items[$delta]->country_code;
    }
    elseif (isset($default_values['country_code'])) {
      $country_code = $default_values['country_code'];
    }

    $element['country_code'] = [
      '#title' => t('Country'),
      '#type' => 'select',
      '#options' => !empty($countries) ? $this->getCountryOptions($countries) : $this->getCountryOptions(),
      '#default_value' => $country_code,
      '#required' => $element['#required'],
    ];

    // Street name.
    $street_name = NULL;
    if (isset($items[$delta]->street_name)) {
      $street_name = $items[$delta]->street_name;
    }
    elseif (isset($default_values['street_name'])) {
      $street_name = $default_values['street_name'];
    }

    $element['street_name'] = [
      '#title' => t('Street Name'),
      '#type' => 'textfield',
      '#default_value' => $street_name,
      '#required' => $element['#required'],
    ];

    // Street number.
    $street_number = NULL;
    if (isset($items[$delta]->street_number)) {
      $street_number = $items[$delta]->street_number;
    }
    elseif (isset($default_values['street_number'])) {
      $street_number = $default_values['street_number'];
    }

    $element['street_number'] = [
      '#title' => t('Street Number'),
      '#type' => 'textfield',
      '#default_value' => $street_number,
      '#required' => $element['#required'],
    ];

    // Postal code.
    $postal_code = NULL;
    if (isset($items[$delta]->postal_code)) {
      $postal_code = $items[$delta]->postal_code;
    }
    elseif (isset($default_values['postal_code'])) {
      $postal_code = $default_values['postal_code'];
    }

    $element['postal_code'] = [
      '#title' => t('Postal code'),
      '#type' => 'textfield',
      '#default_value' => $postal_code,
      '#required' => $element['#required'],
    ];

    // Sub locality.
    $sub_locality = NULL;
    if (isset($items[$delta]->sub_locality)) {
      $sub_locality = $items[$delta]->sub_locality;
    }
    elseif (isset($default_values['sub_locality'])) {
      $sub_locality = $default_values['sub_locality'];
    }

    if (in_array('sub_locality', $optional_fields)) {
      $element['sub_locality'] = [
        '#title' => t('District'),
        '#type' => 'textfield',
        '#default_value' => $sub_locality,
      ];
    }
    else {
      $element['sub_locality'] = [
        '#type' => 'value',
        '#default_value' => FALSE,
      ];
    }

    // Locality.
    $locality = NULL;
    if (isset($items[$delta]->locality)) {
      $locality = $items[$delta]->locality;
    }
    elseif (isset($default_values['locality'])) {
      $locality = $default_values['locality'];
    }

    $element['locality'] = [
      '#title' => t('City'),
      '#type' => 'textfield',
      '#default_value' => $locality,
      '#required' => $element['#required'],
    ];

    // Locked.
    $locked = FALSE;
    if (isset($items[$delta]->locked)) {
      $locked = $items[$delta]->locked;
    }
    elseif (isset($default_values['locked'])) {
      $locked = $default_values['locked'];
    }

    $checkbox_name = $items->getName() . '[' . $delta . '][locked]';
    $element['locked'] = [
      '#title' => t('Provide custom coordinates'),
      '#description' => t('The provided address will be geocoded by default, only check this box if you want to manually provide the coordinates for this location.'),
      '#type' => 'checkbox',
      '#default_value' => $locked,
      '#return_value' => TRUE,
    ];

    // Locked latitude.
    $locked_latitude = FALSE;
    if (isset($items[$delta]->latitude)) {
      $locked_latitude = $items[$delta]->latitude;
    }
    elseif (isset($default_values['latitude'])) {
      $locked_latitude = $default_values['latitude'];
    }

    $element['locked_latitude'] = [
      '#title' => t('Latitude'),
      '#type' => 'textfield',
      '#default_value' => $locked_latitude,
      '#states' => [
        'visible' => [
          ':input[name="' . $checkbox_name . '"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Locked longitude.
    $locked_longitude = FALSE;
    if (isset($items[$delta]->longitude)) {
      $locked_longitude = $items[$delta]->longitude;
    }
    elseif (isset($default_values['longitude'])) {
      $locked_longitude = $default_values['longitude'];
    }

    $element['locked_longitude'] = [
      '#title' => t('Longitude'),
      '#type' => 'textfield',
      '#default_value' => $locked_longitude,
      '#states' => [
        'visible' => [
          ':input[name="' . $checkbox_name . '"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['#element_validate'][] = [get_class($this), 'validateElement'];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {

    // When latitude and longitude are locked. The user provides his own
    // values for both fields, so we need to validate them.
    if (isset($element['locked']) && $element['locked']['#value']) {
      if (!self::validateLatitude($element['locked_latitude']['#value'])) {
        $form_state->setError($element['locked_latitude'], t('Invalid latitude value provided.'));
      }

      if (!self::validateLongitude($element['locked_longitude']['#value'])) {
        $form_state->setError($element['locked_longitude'], t('Invalid longitude value provided.'));
      }
    }
  }

  /**
   * Validates a given latitude.
   *
   * @param float|int|string $lat
   *   Latitude value.
   *
   * @return bool
   *   `true` if $lat is valid, `false` if not
   */
  public static function validateLatitude($lat) {
    return preg_match('/^(\+|-)?(?:90(?:(?:\.0{1,13})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,13})?))$/', $lat);
  }

  /**
   * Validates a given longitude.
   *
   * @param float|int|string $long
   *   Longitude value.
   *
   * @return bool
   *   `true` if $long is valid, `false` if not
   */
  public static function validateLongitude($long) {
    return preg_match('/^(\+|-)?(?:180(?:(?:\.0{1,13})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,13})?))$/', $long);
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $new_values = [];

    foreach ($values as $delta => $value) {
      $location_name = isset($value['location_name']) ? $value['location_name'] : NULL;
      if ($location_name) {
        $new_values[$delta]['location_name'] = $location_name;
      }

      $country_code = isset($value['country_code']) ? $value['country_code'] : NULL;
      if ($country_code) {
        $new_values[$delta]['country_code'] = $country_code;
      }

      $street_name = isset($value['street_name']) ? $value['street_name'] : NULL;
      if ($street_name) {
        $new_values[$delta]['street_name'] = $street_name;
      }

      $street_number = isset($value['street_number']) ? $value['street_number'] : NULL;
      if ($street_number) {
        $new_values[$delta]['street_number'] = $street_number;
      }

      $postal_code = isset($value['postal_code']) ? $value['postal_code'] : NULL;
      if ($postal_code) {
        $new_values[$delta]['postal_code'] = $postal_code;
      }

      $sub_locality = isset($value['sub_locality']) ? $value['sub_locality'] : NULL;
      if ($sub_locality) {
        $new_values[$delta]['sub_locality'] = $sub_locality;
      }

      $locality = isset($value['locality']) ? $value['locality'] : NULL;
      if ($locality) {
        $new_values[$delta]['locality'] = $locality;
      }

      $locked = isset($value['locked']) ? $value['locked'] : FALSE;
      $new_values[$delta]['locked'] = $locked;

      if ($locked) {
        $latitude = isset($value['locked_latitude']) ? $value['locked_latitude'] : NULL;
        if ($latitude) {
          $new_values[$delta]['latitude'] = $latitude;
        }

        $longitude = isset($value['locked_longitude']) ? $value['locked_longitude'] : NULL;
        if ($longitude) {
          $new_values[$delta]['longitude'] = $longitude;
        }
      }
    }

    return $new_values;
  }

}
