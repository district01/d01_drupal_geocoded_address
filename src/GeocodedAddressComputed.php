<?php

namespace Drupal\d01_drupal_geocoded_address;

use Drupal\Core\TypedData\TypedData;

/**
 * A computed property for geocoded address.
 */
class GeocodedAddressComputed extends TypedData {

  /**
   * Cached computed geocoded Address object.
   *
   * @var \Drupal\d01_drupal_geocoded_address\GeocodedAddressInterface|null
   */
  protected $geocodedAddress = NULL;

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if ($this->geocodedAddress !== NULL) {
      return $this->geocodedAddress;
    }

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    $item = $this->getParent();

    $this->geocodedAddress = $item->toGeocodedAddress();
    return $this->geocodedAddress;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    $this->geocodedAddress = $value;
    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->geocodedAddress);
    }
  }

}
