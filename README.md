# d01_drupal_geocoded_address #

Provides an address field that will try to geocode the address on save and store it in the database.
The field is based on the properties provided by the **Geocoder\Model\Address** and extended with a location name.

The value callback of the field will provide a computed address object which has a format method to print the object
and several methods to get the other properties like lat and lon.

# Usage #

Add field through the UI to any entity. 

When used in preprocess of formatter you can format the field this way:

* Location Name: %N
* Street Number: %n
* Street Name: %S
* City (Locality): %L
* City District (Sub-Locality): %D
* Zipcode (Postal Code): %z
* Country: %C
* Country Code: %c
* Timezone: %T

```

$address = $entity_helper->setType('general')->getValue($entity, 'field_address');
$address_line = $address ? $address->format('%S %n, %z %L') : FALSE;

```

Accessing the lat/lon:

```

$address = $entity_helper->setType('general')->getValue($entity, 'field_address');
$lat = $address ? $address->getLatitude() : FALSE;
$lon = $address ? $address->getLongitude() : FALSE;

```


# Release notes #

`1.0`
* Provide a field, widget and formatter for geocoded address.

`2.0`
* Move config to field settings and handle default values.
* Update comments.
* Use DI everywhere

`3.0.0`
* Switch to semantic versioning.
* Move actual geocoding from widget to presave.
* Limit providers hardcoded for now to prevent using useless geocoder services that return incorrect data.
* Provide geocoding route for json api

`3.0.1`
* Provide a setValue() method on field and support passing a GeocodedAddress Object to set the value.
* add readme.
 
`4.0.0`
* Provide a computed spatial field that contains "lat,lon" for solr spatial searches.
* Provide a getSpatial() method on Geocoded Address.

`4.1.0`
* Provide an extra "locked" field in schema.
* Prevent geocoding in preSave() when "locked" is set to TRUE.
* Update widget to support custom coordinates via "locked" field.

`4.1.1`
* Fix issue with validation on locked coordinates. 
* Fix issue with order of lat and lon in getSpatial() function.

`4.1.2`
* Fix passing geocoding provider arguments to make sure settings like API key, ... are passed to provider.

`4.1.3`
* Fixed not being able to save more than 1 address, when using a cardinality > 1

`4.1.4`
* Fixed an issue where provider settings were not saved, nor used during geocoding.

`4.1.5`
* Don't require a street number to have a valid address.

`4.1.5`
* Fixed Drupal 9 compatibility.

`4.1.6`
* Upgrade drupal/geocoder dependency.

`5.0`
* Drupal 9 + geocoder ^3.20 compatibility. 

`5.1`
* Checking feedback style and print warning when needed.
* Updated removed methods from Geocoder 4.x